# fixed_scale_digitize.py

This version if `digitize.py` has the ability to apply a fixed scaling to the 32bit floating point input data in the conversion to 8bit integers.

Use with `./digitize.py -o . -f 4096 ../data/L2037433_SAP000_B000_S0_P000_bf.h5`.